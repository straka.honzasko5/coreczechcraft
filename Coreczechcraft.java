package ml.strakaja.coreczechcraft;

mport java.util.logging.Level;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.boss.*;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Main extends JavaPlugin implements Listener{
    private BossBar msg;

    @Override
    public void onEnable()
    {
        msg = Bukkit.createBossBar("Czechcraft - SEASON BETA",BarColor.GREEN,BarStyle.SOLID);
        getServer().getPluginManager().registerEvents(this, this);
        System.out.println("[Plugin]coreCzechcraft loaded successfuly!");
    }
    @Override
    public void onDisable()
    {
        System.out.println("[coreCzechcraft]Bye,see you later!");
    }
    public boolean onCommand(CommandSender sender,Command cmd,String label,String[] args)
    {
        if(label.equals("spawn"))
        {
            if(sender instanceof Player)
            {
                Player player = (Player)sender;
                Location spawn = getServer().getWorld("world").getSpawnLocation();
                player.teleport(spawn);
                return true;
            }
        }
        return false;
    }
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        event.setJoinMessage(ChatColor.DARK_RED +event.getPlayer().getDisplayName() + " " + ChatColor.WHITE +"se k nám" + ChatColor.BLUE + " připojil/la" + ChatColor.WHITE);
        Player player = event.getPlayer();
        msg.addPlayer(player);
        if(player.getName().equals("Honzasko"))
        {
            player.setPlayerListName("[" + ChatColor.GOLD + "Majitel" + ChatColor.WHITE + "]" + player.getDisplayName());
            player.setDisplayName("[" + ChatColor.GOLD + "Majitel" + ChatColor.WHITE + "]" + player.getDisplayName());
        }
        else
        {
            player.setPlayerListName("[" + ChatColor.AQUA + "Člen" + ChatColor.WHITE + "]" + player.getDisplayName());
            player.setDisplayName("[" + ChatColor.AQUA + "Člen" + ChatColor.WHITE + "]" + player.getDisplayName());
        }
    }
    @EventHandler
    public void onChat(AsyncPlayerChatEvent event)
    {
        Player player  = event.getPlayer();
        if(player.getName().equals("Honzasko"))
        {
            event.setFormat("[" + ChatColor.GOLD + "Majitel" + ChatColor.WHITE + "]" + player.getName() + ": " + event.getMessage());
        }
        else
        {
            event.setFormat("[" + ChatColor.AQUA + "Člen" + ChatColor.WHITE + "]" + player.getName() + ": " + event.getMessage());
        }
    }
}
